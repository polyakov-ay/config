CREATE DATABASE demo;
USE demo;
CREATE TABLE counters (id INT unsigned NOT NULL AUTO_INCREMENT, name varchar(32), value int, PRIMARY KEY (`id`), UNIQUE KEY `name` (`name`));
INSERT INTO counters (name, value) VALUES ('visits', 0);
